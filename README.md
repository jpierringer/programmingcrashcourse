# Javascript development using only the Chrome Web Browser

1. Create a folder dev/www
	* 1a. open terminal on Mac, or Command on Windows
	* 1b. mkdir dev
	* 1c. cd dev
	* 1d. mkdir www
	* 1e. cd www
	* 1f. pwd (this command outputs your "present working directory")
	* 1g. copy that output to the clipboard, or remember it for step 3
	
2. Install the following Chrome extensions:
    * 2a. Caret - Text Editor
    * 2b. Web Server for Chrome - Web server to run javascript at 127.0.0.1:8887

3. configure Web Server
    * 3a. Enter URL: chrome://apps
    * 3b. Click 'Web Server' to launch app
    * 3c. Click [CHOOSE FOLDER] and either paste or enter the path from step 1g. I will refer to this as %web_path% from now on.
    * 3d. Check 'Run in background'
    * 3e. Check 'Start on login'
    * 3f. Check 'Automatically show index.html'
    * 3g. Close and Open Chrome browser (This will launch the web server in the background)

4. Visit URL [Spinnygun example game from Emanuel Ferontato using Phaser IO framework](https://www.emanueleferonato.com/2019/03/30/build-a-html5-game-like-mobile-smashing-hit-spinny-gun-with-no-physics-engines-using-phaser-3-and-paths/)
5. Near the bottom of the page click the link "Download the source code"
6. Extract zipped contents to %web_path%/spinnygun
7. Enter web address: `http://127.0.0.1:8887`
8. Click: 'spinnygun' to launch your game!

That's it! You just launched your first Javascript game. The secret sauce is in the Phaser.io game engine included with the example.

It's time to dig into the code!
We can start in the kiddie pool.

9. Change some configuration settings
    * 9a. Launch Caret (find it at chrome://apps)
    * 9b. Open the file %web_path%/spinnygun/game.js
    * 9c. Change some numbers in the gameOptions object.
    * 9d. Reload the game (press F5) to see how it changes the look or the behavior of your game.

10. Let's make it more like a game by adding a score box using Phaser.Text.
11. Now make it more fun by keeping track of hits in a row.
12. How about a multiplier?
13. Load up the other two games and give them a test drive.
14. How could you improve one of those games to make it your own?
15. Create a new game object
    * 15a. Use a graphics tool to create a new game element and add it to the game folder.
    * 15b. Create a javascript object to control the behavior of your new game element.


**Look at the [API for Phaser 3](https://phaser.io/phaser3/api) for information on how to do it! (I will help if you need it)**
